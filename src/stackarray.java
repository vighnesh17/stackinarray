public class stackarray {
    static final int MAX_SIZE=30;
    int arr[]=new int[MAX_SIZE];
    int top;
    stackarray(){
        top=-1;
    }
    public void push(int val) {
        if (top < MAX_SIZE) {
            arr[++top] = val;
        } else {
            throw new IndexOutOfBoundsException("array overflow");//if array is full
        }
    }
    public int pop(){
        if(top==-1)
            throw new IndexOutOfBoundsException("array underflow");//if array is empty
        return arr[top--];
    }
    public int peek(){
        return arr[top];
    }
}
